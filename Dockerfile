FROM golang:latest AS build
WORKDIR /src/
COPY . ./
RUN CGO_ENABLED=0 go build -o /bin/backend

FROM certbot/certbot:latest
RUN apk update && apk add ca-certificates shadow libcap && rm -rf /var/cache/apk/*
COPY --from=build /bin/backend /backend
RUN setcap 'cap_net_bind_service=+ep' /backend # Allow server to bind privileged ports

EXPOSE 3030
ENTRYPOINT ["/backend"]
