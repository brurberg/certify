package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/gin-gonic/gin"
)

type Domain struct {
	Domain string `json:"domain"`
}

func main() {
	fmt.Println("vim-go")
	quit := make(chan struct{})
	go func() {
		now := time.Now()
		if now.Hour() >= 1 {
			now = now.AddDate(0, 0, 1)
		}
		now = time.Date(now.Year(), now.Month(), now.Day(), 1, 0, 0, 0, now.Location())
		time.Sleep(time.Until(now))
		ticker := time.NewTicker(24 * time.Hour)
		for {
			select {
			case <-ticker.C:
				err := renewCerts()
				if err != nil {
					log.Println(err) //TODO: Change this to a mail warning
				}
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
	r := gin.Default()
	r.POST("/addNewDomain", func(c *gin.Context) {
		var d Domain
		err := c.BindJSON(&d)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		err = getNewCert(d.Domain)
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		flushCerts()
		c.JSON(200, gin.H{
			"message": "success",
		})
	})
	r.POST("/renew", func(c *gin.Context) {
		err := renewCerts()
		if err != nil {
			log.Println(err)
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		flushCerts()
		c.JSON(200, gin.H{
			"message": "success",
		})
	})
	r.Run(":3000")
}

func getNewCert(domene string) (err error) {
	cmd := exec.Command("certbot", "certonly", "--webroot", "-n", "--agree-tos", "--webroot-path", "/mnt/certbot", "--config-dir", "/mnt/letsencrypt", "--work-dir", "/mnt/work", "--logs-dir", "/mnt/logs", "-m", "sindre@brurberg.no", "--domains", domene)
	err = cmd.Run()
	return
}

func renewCerts() (err error) {
	cmd := exec.Command("certbot", "renew", "--webroot", "-n", "--agree-tos", "--webroot-path", "/mnt/certbot", "--config-dir", "/mnt/letsencrypt", "--work-dir", "/mnt/work", "--logs-dir", "/mnt/logs", "-m", "sindre@brurberg.no")
	err = cmd.Run()
	return
}

func flushCerts() (err error) {
	req, err := http.NewRequest("POST", "https://orch.brurberg.no/api/restartgateway", nil)
	if err != nil {
		log.Println("Error reading request. ", err)
		return
	}

	req.Header.Set("authorization", os.Getenv("GATEWAY_SECRET"))
	client := &http.Client{}

	_, err = client.Do(req)
	if err != nil {
		log.Println("Error reading response. ", err)
	}
	return
}
